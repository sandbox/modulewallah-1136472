<?php

/**
 * Implements hook_block().
 */
function bigpublishbutton_block($op = 'list', $delta = 0, $edit = array()) {
  if ($op == 'list') {
    $blocks['bigpublishbutton_block'] = array(
      'info' => t('A form with a big button to publish the current node and referenced nodes'),
    );
    return $blocks;
  }
  elseif ($op == 'view') {
    switch ($delta) {
      case 'bigpublishbutton_block':
      default:
        $block = bigpublishbutton_block_build();
        break;
    }
    return $block;
  }
}

/**
 * A block callback function.
 */
function bigpublishbutton_block_build() {
  // If they aren't a node admin, get out of here.
  if (!user_access('administer nodes')) {
    // TODO: Consider making this a custom permission, but that can get messy.
    return;
  }

  // Only show this if they are looking at a node.
  if ($node = menu_get_object('node')) {
    if (!empty($node->nid)) {
      $content = drupal_get_form('bigpublishbutton_button_form', $node->nid);
      $subject = t('Publish this content');
      return array('subject' => $subject, 'content' => $content);
    }
  }
}

/**
 * A form for publishing a node.
 * The block is responsible for checking access, not this form.
 */
function bigpublishbutton_button_form($form_state, $nid) {
  $form = array();
  $form['nid'] = array('#type' => 'value', '#value' => $nid);

  $form['publish'] = array(
    '#type' => 'submit',
    '#value' => t('Publish'),
    '#submit' => array('bigpublishbutton_publish_submit')
  );
  $form['unpublish'] = array(
    '#type' => 'submit',
    '#value' => t('Un-publish'),
    '#submit' => array('bigpublishbutton_unpublish_submit')
  );

  return $form;
}

/**
 * Submit handler for the bigpublishbutton form: publish action.
 */
function bigpublishbutton_publish_submit($form, &$form_state) {
  bigpublishbutton_take_action($form_state['values']['nid'], 'publish');
}

/**
 * Submit handler for the bigpublishbutton form: UNpublish action.
 */
function bigpublishbutton_unpublish_submit($form, &$form_state) {
  bigpublishbutton_take_action($form_state['values']['nid'], 'unpublish');
}

/*
 * A function for publishing or unpublishing nodes.
 *
 * Note that whatever function calls this must do its own access checking. This
 * is purely an API.
 *
 * @param $nid integer node ID of the node to publish.
 * @param $action string whether to "publish" or "unpublish".
 */
function bigpublishbutton_take_action($nid, $action) {
  // Set the actions and confirm they provided one of two actions supported.
  switch ($action) {
    case 'publish':
      $actions = array('node_publish_action', 'node_save_action');
      break;
    case 'unpublish':
      $actions = array('node_unpublish_action', 'node_save_action');
      break;
    default:
      return;
  }

  // Get the node.
  $node = node_load($nid);
  // Quick sanity check.
  if (isset($node->nid)) {
    $nids = bigpublishbutton_get_referenced_nids($node);
    // We add this on last to publish this story after it's sub stories have been published.
    $nids[] = $node->nid;
    foreach ($nids as $nid) {
      $current_node = node_load($nid);
      $context['node'] = $current_node;
      actions_do($actions, $current_node, $context, NULL, NULL);
    }

  }

}

/**
 * Helper function to find referenced nids.
 * @param unknown_type $node
 */
function bigpublishbutton_get_referenced_nids($node) {
  // See if there are nodereferences on this site.
  if (function_exists('content_types')) {
    $data = content_types($node->type);
    foreach ($data['fields'] as $field_name => $field_data) {
      // If a field is a noderef, grab its data and find the nids.
      if ($field_data['type'] == 'nodereference') {
        $field = $node->$field_name;
        // Handle multi-value fields.
        foreach ($field as $index => $data) {
          if (!empty($data['nid'])) {
            $nids[] = $data['nid'];
          }
        }
      }
    }
  }
  return $nids;
}
