This module provides a block that will show on node pages. The block has a button (well, two buttons) for "Publish" and "Un-publish."

When clicked these buttons will not only publish/unpublish the current node but also all nodes referenced by the current node.

This feature is intended to be especially useful for sites based on a "newsletter" or "issues" concept like a magazine. Consider this scenario/recipe:

You create two content types: story and issue. The issue has a nodereference field to story nodes.
Story and issue nodes default to a not-published state.
Contributors create story nodes and an editor creates a new issue node that references stories.
Contributors and the editor collaborate on stories to get them ready for publishing
Once the issue and stories are ready, the editor wants to publish them all at the same time...but there is no easy tool to achieve this
Add the bigpublishbutton.module to the site, and enable the block in a sidebar. Now the editor can click a single button and publish all the nodes at once

### Recommended and similar modules

The Fasttoggle module provides a much easier way to publish individual nodes.
http://drupal.org/project/fasttoggle

If you are using this you probably also want to use CCK nodereferences.
http://drupal.org/project/cck
